const Firebase = require("../../../Speaker/Firebase/");
let EstablishConnection = require("../EstablishConnection/index.js");

function tgSignIn(app_conf, DC, auth_key, phone_number, phone_code_hash, phone_code, speakerId){
    return new Promise((resolve, reject) => {
        EstablishConnection.checkAuth(app_conf, auth_key)
            .then((conf)=> {
                return EstablishConnection.getClient(conf, DC);
            })
            .then((client)=> {
                client.auth.signIn(phone_number, phone_code_hash, phone_code, (result)=>{
                    Firebase.DataBase.updateData(`${speakerId}/tg`, {[phone_number]: {"authKey": auth_key, "firstName": result.user.first_name,"lastName":result.user.last_name, "username":result.user.username}})
                        .then(() => {
                            resolve(result);
                        });
                });
            })
            .catch((error) => {
                reject(error);
            });
    });
}

module.exports = tgSignIn;
