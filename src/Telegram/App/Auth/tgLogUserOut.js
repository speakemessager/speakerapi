const Firebase = require("../../../Speaker/Firebase/");

let EstablishConnection = require("../EstablishConnection/index.js");

function tgLogOut(app_conf, DC, phoneNumber, auth_key,speakerId){
    return new Promise((resolve, reject) => {
        EstablishConnection.checkAuth(app_conf, auth_key)
            .then((conf)=> {
                return EstablishConnection.getClient(conf, DC);
            })
            .then((client)=> {
                if (client) {
                    Firebase.DataBase.deleteData(`${speakerId}/tg`, phoneNumber)
                        .then(() => {
                            resolve({"success": "1"});
                        });
                } else {
                    reject({"success": "0", "error": {"message": "client was not confirmed "}})
                }

            })
            .catch((error) => {
                reject(error);
            });
    });
}

module.exports = tgLogOut;
