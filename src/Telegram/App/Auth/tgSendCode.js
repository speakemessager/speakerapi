const Firebase = require("../../../Speaker/Firebase/");
let EstablishConnection = require("../EstablishConnection/index.js");

function tgSendCode(app_conf, DC, phone_number, sms_type, lang_code, speakerId){
    return new Promise((resolve, reject) => {
        EstablishConnection.setAuth(app_conf, DC)
            .then((authObj) => {
                return Firebase.DataBase.updateData(`${speakerId}/tg`, {[phone_number]: {"authKey": authObj.appKey}}, authObj);
            })
            .then((authObj)=> {
                /*TODO think how to rewrite method below with promises.
             Hint: there is no errback contract in current callback, anything to reject*/
                authObj.client.auth.sendCode(phone_number, sms_type, lang_code, (result)=>{
                    resolve({
                        result,
                        "authKey" : authObj.appKey
                    });
                });
            })
            .catch((error) => {
                reject(error);
            });
    });
}

module.exports = tgSendCode;
