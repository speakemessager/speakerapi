let sendCode = require("./tgSendCode");
let signIn = require("./tgSignIn");
let signUp = require("./tgSignUp");
let logOut = require("./tgLogUserOut");

function Account(app_conf, DC){
    return function(auth_key){
        return {
            tgSendCode(phone_number, sms_type, lang_code, speakerId){
                return sendCode(app_conf, DC, phone_number, sms_type, lang_code, speakerId);
            },
            tgSignIn(phone_number,phone_code_hash, phone_code, speakerId){
                return signIn(app_conf, DC, auth_key, phone_number, phone_code_hash, phone_code, speakerId);
            },
            tgSignUp(phoneNumber, phoneCode, phoneCodeHash, FirstName, LastName, speakerId){
                return signUp(app_conf, DC, auth_key, phoneNumber, phoneCode, phoneCodeHash, FirstName, LastName, speakerId);
            },
            tgLogOut(phoneNumber, speakerId){
                return logOut(app_conf, DC, phoneNumber, auth_key, speakerId);
            }
        };
    };
}

module.exports = Account;
