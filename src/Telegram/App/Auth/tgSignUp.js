const Firebase = require("../../../Speaker/Firebase/");
let EstablishConnection = require("../EstablishConnection/index.js");

function tgSignUp(app_conf, DC, auth_key, phoneNumber, phoneCode, phoneCodeHash, FirstName, LastName, speakerId){
    return new Promise((resolve, reject) => {
        EstablishConnection.checkAuth(app_conf, auth_key)
            .then((conf)=> {
                return EstablishConnection.getClient(conf, DC);
            })
            .then((client)=> {
                client.auth.signUp(phoneNumber, phoneCodeHash, phoneCode, FirstName, LastName, (result)=>{
                    Firebase.DataBase.updateData(`${speakerId}/tg`, {[phoneNumber]: {"authKey": auth_key, "firstName": result.user.first_name,"lastName":result.user.last_name, "username":result.user.username}})
                        .then(() => {
                            resolve(result);
                        });
                });
            })
            .catch((error) => {
                reject(error);
            });
    });
}

module.exports = tgSignUp;
