let EstablishConnection = require("../EstablishConnection/index.js");

function getContacts(app_conf, DC, auth_key, hash){
    return new Promise((resolve, reject) => {
        EstablishConnection.checkAuth(app_conf, auth_key)
            .then((conf)=> {
                return EstablishConnection.getClient(conf, DC);
            })
            .then((client)=> {
                client.contacts.getContacts(hash, (result)=>{
                    resolve(result);
                });
            })
            .catch((error) => {
                reject(error);
            });
    });
}

module.exports = getContacts;
