const tgGetContacts = require("./getContacts");


function Contacts(app_conf, DC){
    return function(auth_key){
        return {
            getContacts(hash){
                return tgGetContacts(app_conf, DC, auth_key, hash);
            }
        };
    };
}

module.exports = Contacts;
