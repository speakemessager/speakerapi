// let config = require("config");
let telegramLink = require("telegram.link")();

const appId = 156677;
const appHash = "73a1894326349091c84317cf5ffc7a86";

let app_conf = {
    id: appId,
    hash: appHash,
    version: "0.0.1",
    lang: "en"
};
let DC = telegramLink.PROD_PRIMARY_DC;
//let DC = telegramLink.TEST_PRIMARY_DC;

if (process.env.NODE_ENV) {
    DC = (process.env.NODE_ENV.trim() === "development") ?  telegramLink.TEST_PRIMARY_DC : DC;
}

module.exports = {
    Auth : require("./Auth/index")(app_conf, DC),
    Contacts : require("./Contacts/index")(app_conf,DC)
};