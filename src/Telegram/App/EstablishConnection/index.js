
let config = require("config");
let telegramLink = require("telegram.link");
let publicKey = config.get("EstablishConnectionPass.public");


module.exports.checkAuth = function(app_conf, authKey) {
    return new Promise((resolve, reject) => {
        if (app_conf && authKey) {
            let conf = app_conf;
            if (authKey) {
                conf.authKey = telegramLink.retrieveAuthKey(new Buffer(authKey, "base64"), publicKey);
            } else
                reject(new Error("checkAuth failed"));
            resolve(conf);
        } else {
            reject(new Error("Not enough parametrs in checkAuth"));
        }
    });
};


module.exports.getClient = function (app, DC){
    return new Promise((resolve, reject) => {
        if (app && DC){
            let client = telegramLink.createClient(app, DC, (error) => {
                if (error) reject(error);
                try {
                    if (client.isReady(true)) {
                        resolve(client);
                    }
                } catch(e){
                    reject(new Error("Client isn't ready! Check authKey"));
                }

            });
        } else {
            reject(new Error("Not enough parametrs in getClient"));
        }
    });
};

module.exports.setAuth = function (app_conf, DC){
    return new Promise((resolve, reject) => {
        createClientPromise(app_conf, DC)
            .then((client) => {
                return createAuthKeyPromise(client);
            })
            .then((resultObj)=> {
                if (resultObj.client.isReady(true)) {
                    let appKey = resultObj.auth.key.encrypt(publicKey).toString("base64");
                    resolve({
                        "client" : resultObj.client,
                        appKey
                    });
                }
            })
            .catch((error) => {
                reject(error);
            });
    });
};

function createClientPromise(app_conf, DC){
    return new Promise((resolve,reject)=>{
        let client = telegramLink.createClient(app_conf, DC, (error)=>{
            if (error) {
                reject(error);
            }  else {
                resolve(client);
            }
        });
    });
}

function createAuthKeyPromise(client){
    return new Promise((resolve,reject)=> {
        client.createAuthKey((auth) => {
            if (auth instanceof Error) {
                reject(auth);
            } else {
                resolve({
                    client,
                    auth
                });
            }

        });

    });
}

