let Telegram = require("./App");
let response = require("./../Responses/response");
/** @module speaker/telegram */

module.exports = function (app) {

    /* Authorization */
    /**
     *  @method Send code to the telephone
     *  @description /tgSendCode - Path to server
     *  @param {string} req.body.phoneNumber - User phone
     *  @param {string} req.body.speakerToken - Unique speaker user token
     *  @returns {Json} Return json with telegram settings: important authKey, phone_code_hash
     */
    app.post("/tgSendCode", (req, res) => {
        if (req.body.phoneNumber &&
            req.body.speakerToken) {
            let {phoneNumber, speakerToken} = req.body;
            Telegram.Auth().tgSendCode(phoneNumber, 0, "en", speakerToken)
                .then((toClientObj) => {
                    res.json({
                        "result": toClientObj.result,
                        "authKey": toClientObj.authKey,
                        "speakerStatusCode": "200"
                    });
                })
                .catch((error) =>
                    response.wrongAuth(res, error.message)
                );
        } else {
            response.wrongParams(res, "Phone Number wasn't sent");
        }
    });
    /**
     *  @method Sign in user into the telegram
     *  @description /tgSignIn - Path to server
     *  @param {string} req.body.phoneNumber - User phone
     *  @param {string} req.body.phoneCode - code that was sent by Telegram to the phone
     *  @param {string} req.body.phoneCodeHash - phone_code_hash in response for /tgSendCode
     *  @param {string} req.body.authKey - authKey in response for /tgSendCode
     *  @param {string} req.body.speakerToken - Unique speaker user token
     *  @returns {Json} Return son with user info
     */
    app.post("/tgSignIn", (req, res)=> {
        if (req.body.phoneCode &&
            req.body.phoneNumber &&
            req.body.authKey &&
            req.body.phoneCodeHash &&
            req.body.speakerToken){
            let {phoneCode, phoneNumber, authKey, phoneCodeHash, speakerToken} = req.body;
            Telegram.Auth(authKey).tgSignIn(phoneNumber, phoneCodeHash, phoneCode, speakerToken)
                .then((resultObj) => {
                    resultObj.statusSpeakerCode="200";
                    res.json(resultObj);
                })
                .catch((error) =>
                    response.wrongAuth(res, error.message)
                );
        } else
            response.wrongParams(res);
    });
    /**
     *  @method Sign up user into the telegram
     *  @description /tgSignUp - Path to server
     *  @param {string} req.body.phoneNumber - User phone
     *  @param {string} req.body.phoneCode - code that was sent by Telegram to the phone
     *  @param {string} req.body.phoneCodeHash - phone_code_hash in response for /tgSendCode
     *  @param {string} req.body.authKey - authorization Key in response for /tgSendCode
     *  @param {string} req.body.firstName - user's First Name
     *  @param {string} req.body.lastName - user's Last Name
     *  @param {string} req.body.speakerToken - Unique speaker user token
     *  @returns {Json} Return json with user info
     */
    app.post("/tgSignUp", (req, res) => {
        if (req.body.authKey &&
            req.body.phoneNumber &&
            req.body.phoneCode &&
            req.body.phoneCodeHash &&
            req.body.firstName &&
            req.body.lastName &&
            req.body.speakerToken){
            let {phoneNumber, phoneCode, phoneCodeHash, firstName, lastName,authKey} = req.body;
            Telegram.Auth(authKey).tgSignUp(phoneNumber, phoneCode, phoneCodeHash, firstName, lastName)
                .then((resultObj) => {
                    resultObj.statusSpeakerCode="200";
                    res.json(resultObj);
                })
                .catch((error) =>
                    response.wrongAuth(res, error.message)
                );
        } else
            response.wrongParams(res);
    });
    /**
     *  @method Log out user from telegram
     *  @description /tgLogOut - Path to server
     *  @param {string} req.body.phoneNumber - User phone
     *  @param {string} req.body.authKey - authKey in response for /tgSendCode
     *  @param {string} req.body.speakerToken - Unique speaker user token
     *  @returns {Json} Return json with statusSpeakerCode 200
     */
    app.post("/tgLogOut", (req, res) => {
        if (req.body.authKey &&
            req.body.phoneNumber &&
            req.body.speakerToken){
            let {phoneNumber, authKey, speakerToken} = req.body;
            Telegram.Auth(authKey).tgLogOut(phoneNumber, speakerToken)
                .then((resultObj) => {
                    resultObj.statusSpeakerCode="200";
                    res.json(resultObj);
                })
                .catch((error) =>
                    response.wrongAuth(res, error.message)
                );
        } else
            response.wrongParams(res);
    });

    /* Contacts */
    /**
     *  @method Get contacts from telegram
     *  @description /tgGetContacts - Path to server
     *  @param {string} req.body.authKey - authorization key in response for /tgSendCode
     *  @param {string} [req.body.hash=""] - hash: https://core.telegram.org/method/contacts.getContacts
     *  @returns {Json} Return json with user info
     */
    app.post("/tgGetContacts", function (req, res) {
        if (req.body.authKey) {
            let {authKey, hash = ""} = req.body;
            Telegram.Contacts(authKey).getContacts(hash)
                .then((resultObj) => {
                    resultObj.statusSpeakerCode="200";
                    res.json(resultObj);
                })
                .catch((error) =>
                    response.wrongAuth(res, error.message)
                );
        } else {
            response.wrongParams(res);
        }
    });

};