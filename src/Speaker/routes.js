"use strict";
const responses = require("../Responses/response");
const FireBase =  require("./Firebase/index.js");
const VkUserClass = require("../VK/App/LongPoll");
/** @module speaker/app */

module.exports = function (app, server) {

    /**
     *  @method check Server availability
     *  @description /testServer - Path to server
     *  @returns {Json} Return json with  "statusSpeakerCode" : "200" if everything is OK
     */
    app.all(["/testServer", "/"], (req, res)=>{
        responses.sendOK(res);
        res.end();
    });


    /**
     *  @method Sign In the user with email and password
     *  @description /appSignInEmailPassword - Path to server
     *  @param {string} req.body.email - User Email
     *  @param {string} req.body.password - User Password
     *  @returns {Json} Return Json with user information or Json with Error code
     */
    app.post("/appSignInEmailPassword", (req, res) => {
        if (req.body.email && req.body.password) {
            let {email, password} = req.body;
            FireBase.Account.signInEmailPass(email, password)
                .then(
                    user => {
                        user.statusSpeakerCode = "200"; res.json(user);
                    }
                )
                .catch(
                    error => responses.wrongAuth(res, error.message)
                );
        } else {
            responses.wrongParams(res);
        }
    });

    /**
     *  @method Sign up new user with email and password
     *  @description /appSignUpEmailPassword - Path to server
     *  @param {string} req.body.email - User Email
     *  @param {string} req.body.password - User Password
     *  @returns {Json} Return Json with User Info or Json with Error code
     */
    app.post("/appSignUpEmailPassword", (req, res) => {
        if (req.body.email && req.body.password) {
            let {email, password} = req.body;
            FireBase.Account.signUpEmailPass(email, password)
                .then(
                    user => {
                        user.statusSpeakerCode = "200"; res.json(user);
                    }
                )
                .catch(
                    error => responses.wrongAuth(res, error.message)
                );
        } else {
            responses.wrongParams(res);
        }
    });
    /**
     *  @method Sign out new user with email and password
     *  @description /appSignOutEmailPassword - Path to server
     *  @returns {Json} Return Json with  "statusSpeakerCode" : "200" and "success":"1" or Json with Error code
     */
    app.post("/appSignOutEmailPassword", (req, res) => {
        FireBase.Account.signOutEmailPass()
            .then(
                () => res.json({
                    "statusSpeakerCode" : "200",
                    "success" : "1"
                })
            )
            .catch(
                error => responses.wrongAuth(res, error.message)
            );
    });


    let io = require("socket.io")(server);
    io.on("connection", (socket)=> {
        let user;
        /**
         * Initialize vk LongPoll server for User
         *
         * @event startGettingVkUpdates
         * @property {string} accessToken - access token that can be obtained in /vkLogIn
         */
        socket.on("startGettingVkUpdates", (data)=> {
            if (!user) user = new VkUserClass(data.accessToken, socket);
            if (user)  user.startLongPoll();
        });

        /**
         * Stop VK long poll if not needed or after disconnect
         *
         * @event disconnect
         */
        socket.on("disconnect", ()=> {
            if (user) user.stopLongPoll();
        });
    });

};