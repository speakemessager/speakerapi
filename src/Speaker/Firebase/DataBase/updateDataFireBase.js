let firebaseEncode = require("firebase-encode");

function updateDataFireBase(ref, child, obj, callbackObj){
    return new Promise((resolve, reject) => {
        obj = JSON.parse(firebaseEncode.encode(JSON.stringify(obj)));
        child = child.toLowerCase();
        ref.child(`${child}/${Object.keys(obj)[0]}`).update(obj[Object.keys(obj)[0]])
            .then(() => {
                if (callbackObj) {
                    resolve(callbackObj);
                } else {
                    resolve();
                }
            })
            .catch((error)=> reject(new Error("Something has happened with updateDataFireBase: " + error.message)));
    });
}

module.exports = updateDataFireBase;

