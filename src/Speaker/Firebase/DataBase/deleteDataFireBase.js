const firebaseEncode = require("firebase-encode");


function deleteDataFireBase(ref, child, obj){
    return new Promise((resolve, reject) => {
        obj = JSON.parse(firebaseEncode.encode(JSON.stringify(obj)));
        child = child.toLowerCase();
        ref.child(`${child}/${obj}`).remove()
            .then(()=> resolve())
            .catch((error)=> reject(new Error("Something has happened with deleteDataFireBase: " + error.message)));
    });
}

module.exports = deleteDataFireBase;

