const firebaseEncoder = require("firebase-encode");

function getDataFireBase(ref, child, key){
    return new Promise((resolved, rejected) => {
        key = firebaseEncoder.encode(key);
        child = child.toLowerCase();
        ref.child(`${child}/${key}`).once("value", function(objectBack) {
            if (objectBack) {
                resolved(objectBack);
            } else {
                rejected(new Error("object was empty in getDataFireBase"));
            }
        });
    });
}

module.exports = getDataFireBase;

