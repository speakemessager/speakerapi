let firebaseEncode = require("firebase-encode");

function setDataFireBase(ref, child, obj){
    return new Promise((resolve, reject) => {
        obj = JSON.parse(firebaseEncode.encode(JSON.stringify(obj)));
        child = child.toLowerCase();
        ref.child(`${child}/${Object.keys(obj)[0]}`).set(obj[Object.keys(obj)[0]])
            .then(()=> resolve())
            .catch((error)=> reject(new Error("Something has happened with setDataFireBase: " + error.message)));
    });
}

module.exports = setDataFireBase;

