const setDataFireBase = require("./setDataFireBase");
const updateDataFireBase = require("./updateDataFireBase");
const deleteDataFireBase = require("./deleteDataFireBase");
const getDataFireBase = require("./getDataFireBase");
const currentRefConst = require("config").get("DatabaseRef");

function Database(firebase){

    let ref = firebase.database().ref(currentRefConst);
    return {
        setData(child,obj){
            return setDataFireBase(ref, child, obj);
        },
        updateData(child, obj, callbackObj){
            return updateDataFireBase(ref, child, obj, callbackObj);
        },
        deleteData(child,obj){
            return deleteDataFireBase(ref, child, obj);
        },
        getData(child, obj){
            return getDataFireBase(ref, child, obj);
        }
    };
}



module.exports = Database;
