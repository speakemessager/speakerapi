let firebase = require("firebase");
let userServiceAccount = require("./../../../config/firebaseServiceAccountKey.json");
let appUser = firebase.initializeApp(userServiceAccount);


let firebaseAdmin = require("firebase-admin");
let adminServerAccount = require("./../../../config/firebaseAdminServiceAccount.json");
let appAdmin = firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(adminServerAccount),
    databaseURL: "https://test-27121997.firebaseio.com"

});


module.exports = {
    Account: require("./Account")(appUser),
    DataBase: require("./DataBase")(appAdmin)
};