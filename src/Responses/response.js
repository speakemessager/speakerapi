"use strict";

//404 no found
module.exports.notFound = (res) =>  res.json({
    "statusSpeakerCode" : "404"
});

//200 ok
module.exports.sendOK = (res) =>   res.json({
    "statusSpeakerCode" : "200"
});

//403 bad request wrong Params
module.exports.notReady = module.exports.wrongAuth = (res,errorMsg) => {
    if (errorMsg) {
        res.json({
            "errorSpeakerCode" : errorMsg,
            "statusSpeakerCode" : "403"
        });
    } else {
        res.json({
            "statusSpeakerCode" : "403"
        });
    }
};

module.exports.wrongParams = (res, errorMsg) =>  {
    if (errorMsg) {
        res.json({
            "errorSpeakerCode" : errorMsg,
            "statusSpeakerCode" : "400"
        });
    } else {
        res.json({
            "statusSpeakerCode" : "400"
        });    }
};
