const VKApi = require("node-vkapi");
const config = require("config");

module.exports = function(accessToken) {

    let VK = new VKApi({
        apiVersion:"5.68",
        appid: config.get("VkAuth.appId"),
        appSecret: config.get("VkAuth.privateKey"),
        accessToken
    });

    return {
        Auth : require("./Auth")(VK),
        Call : require("./Call")(VK),
    };
};