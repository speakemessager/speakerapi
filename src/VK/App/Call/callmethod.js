
function LogIn(VK, methodName, params){
    return new Promise((resolve,reject) => {
        VK.call(methodName, params)
            .then((responseObject) => {
                resolve(responseObject);
            })
            .catch( (error)=>{
                reject(error);
            });
    });
}


module.exports = LogIn;