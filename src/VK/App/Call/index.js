const callMethod = require("./callmethod.js");

function CallVKApiModule(VK){
    return {
        method(methodName, params){
            return callMethod(VK, methodName, params);
        }
    };
}

module.exports = CallVKApiModule;
