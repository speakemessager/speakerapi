const config = require("config");
const appId = config.get("VkAuth.appId");
const Firebase = require("../../../Speaker/Firebase");

function LogIn(VK, login, password, speakerId){
    return new Promise((resolve,reject) => {
        VK.logIn({appId, login, password, "scope": "4264991" })
            .then( (authResponseObject) => {
                return Firebase.DataBase.updateData(`${speakerId}/vk`, {[login]: {"accessToken": authResponseObject.access_token, "userId": authResponseObject.user_id}}, authResponseObject);
            })
            .then((authResponseObject) => {
                resolve(authResponseObject);
            })
            .catch( (error)=>{
                reject(error.message);
            });
    });
}


module.exports = LogIn;