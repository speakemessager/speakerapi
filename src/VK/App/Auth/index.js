const LogIn = require("./LogIn");

function AuthModule(VK){
    return {
        vkLogIn(userLogin, userPassword, speakerId){
            return LogIn(VK, userLogin, userPassword, speakerId);
        }
    };
}

module.exports = AuthModule;
