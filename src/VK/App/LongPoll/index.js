const VK = require("../../App");
const request = require("request");

// https://{$server}?act=a_check&key={$key}&ts={$ts}&wait=25&mode=2&version=2

class VkUser {

    constructor(accessToken, currentSocket){
        this.accessToken = accessToken;
        this.longPollStarted = false;
        this.getLongPollObj = null;
        this.updatesArr = [];
        this.socket = currentSocket;
    }

    get getAccessToken() {
        return `${this.accessToken}`;
    }
    _getLongPollServer(){
        return new Promise( (resolve,reject) => {
            VK(this.accessToken).Call.method("messages.getLongPollServer", {"need_pts":1, "Ip_version": 2})
                .then( (obj) => {
                    this.getLongPollObj = obj;
                    resolve(obj);
                })
                .catch( (error) => {
                    reject(error);
                });
        });

    }
    _longPoll(tsArg) {
        let {server, key} = this.getLongPollObj;
        let ts = tsArg || this.getLongPollObj.ts;
        let options = {
            url: `https://${server}?act=a_check&key=${key}&ts=${ts}&wait=25&mode=106&version=2`,
            method: "POST"
        };
        if (this.longPollStarted) {
            this.lastRequest = request(options, (error, response, body)=>{
                if (!error && response.statusCode === 200){
                    body = JSON.parse(body);
                    this.socket.emit("newVkUpdate", JSON.stringify(body));
                    this._longPoll(body.ts);
                } else {
                    throw new Error("Error in VK: longPollStarted: " + error.message);
                }
            });
        } else {
            this.lastRequest.abort();
        }
    }

    startLongPoll(){
        this.longPollStarted = true;
        this._getLongPollServer()
            .then(()=>{
                return this._longPoll();
            });
    }

    stopLongPoll(){
        this.longPollStarted = false;
        this._getLongPollServer()
            .then(() => {
                return this._longPoll();
            });
    }
}


module.exports = VkUser;
