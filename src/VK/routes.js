let VK = require("./App");
let response = require("./../Responses/response");
/** @module speaker/vk */


module.exports = function (app) {

    /**
     *  @method Log in user into VK
     *  @description /vkLogIn - Path to server
     *  @param {string} req.body.userLogin - User login(email or phone)
     *  @param {string} req.body.userPassword - User password
     *  @param {string} req.body.speakerToken - Unique speaker user token
     *  @returns {Json} Return json with vk settings: important accessToken
     */
    app.post("/vkLogIn", (req, res) => {
        if (req.body.userLogin &&
            req.body.userPassword &&
            req.body.speakerToken){
            let {userLogin, userPassword, speakerToken} = req.body;
            VK().Auth.vkLogIn(userLogin, userPassword, speakerToken)
                .then((AuthResponseObject) => {
                    AuthResponseObject.statusSpeakerCode="200";
                    res.json(AuthResponseObject);
                })
                .catch((error) =>
                    response.wrongAuth(res, error.message)
                );
        } else {
            response.wrongParams(res, "Check userLogin and userPassword");
        }
    });

    /**
     *  @method Call any methods for VK
     *  @description /vkCall - Path to server
     *  @param {string} req.body.methodName - Method name according to https://vk.com/dev/methods
     *  @param {string} req.body.accessToken - Access token obtained in /vkLogIn
     *  @param {string} [req.body.`some necessary parameter`] - Difference params that are used in a particular method
     *  @returns {Json} Return json with data
     */
    app.post("/vkCall", (req, res) => {
        if (req.body.methodName &&
            req.body.accessToken){
            let {methodName, accessToken, ...params} = req.body;
            VK(accessToken).Call.method(methodName, params)
                .then((callReturnPromise) => {
                    callReturnPromise.statusSpeakerCode="200";
                    res.json(callReturnPromise);
                })
                .catch((error) =>
                    response.wrongAuth(res, error.message)
                );
        } else {
            response.wrongParams(res, "Check methodName, accessToken or params object");
        }
    });





};