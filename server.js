const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const app = express();
const port = require("config").get("Server.port");
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({extended: false}));
app.use("/docs", express.static(__dirname + "/docs"));

let server = http.createServer(app);
server.listen(process.env.PORT || port);

require("./src/Speaker/routes")(app,server);
require("./src/Telegram/routes")(app);
require("./src/VK/routes")(app);

module.exports = server;


// Below stub to maintain free heroku server. DELETE to the end ON PRODUCTION
setInterval(function(){
    http.get("http://speakertest.herokuapp.com");
    console.log("Not to sleep get request");
},720000);

