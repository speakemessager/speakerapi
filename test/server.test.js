let chai = require("chai");
let chaiHttp = require("chai-http");
let mocha = require("mocha");
let expect = require("chai").expect;
let config = require("config");
let server = require("../server");

chai.use(chaiHttp);
describe("Start Server", () => {

    it("it should return json with statusSpeakerCode 200 if server is OK", function(done) {
        chai.request(`http://localhost:${config.get("Server.port")}`)
            .get("/testServer")
            .end(function (err, res){
                expect(res.body.statusSpeakerCode).to.equal("200");
                done();
            });
    });


});