let chai = require("chai");
let chaiHttp = require("chai-http");
let mocha = require("mocha");
let expect = require("chai").expect;
let config = require("config");
let server = require("../../../server");


chai.use(require('chai-json'));
chai.use(chaiHttp);
describe("tgSendCode tests", () => {

    it("it should send json statusSpeakerCode=400 if phoneNumber wasn't received", function(done) {
        chai.request(`http://localhost:${config.get("Server.port")}`)
            .post("/tgSendCode")
            .end(function (err, res){
                expect(res.body.statusSpeakerCode).to.equal("400");
                expect(res.body.errorSpeakerCode).to.equal("Phone Number wasn't sent");
                done();
            });
    });


    it.skip("it should send json response with authKey and appropriate _typeName", function(done) {
        this.timeout(8000);
        chai.request(`http://localhost:${config.get("Server.port")}`)
            .post("/tgSendCode")
            .send({"phoneNumber" : "380638843866"})
            .end(function (err, res){
                expect(res.body.statusSpeakerCode).to.equal("200");
                expect(res.body.authKey).to.exist;
                expect(res.body.result._typeName).to.equal("api.type.auth.SentCode");
                done();
            });
    });


});