let chai = require("chai");
let chaiHttp = require("chai-http");
let mocha = require("mocha");
let expect = require("chai").expect;
let config = require("config");
let server = require("../../../../server");
let signIn = require("../../../../src/Speaker/Firebase/Account/appSignUpEmailPassword");

chai.use(chaiHttp);
describe("appSignUpEmailPassword", () => {

    it("it should return json statusSpeakerCode=400 when password wasn't sent", function(done) {
        chai.request(`http://localhost:${config.get("Server.port")}`)
            .post("/appSignUpEmailPassword")
            .send({
                "email":"test@gmail.com",
            })
            .end(function (err, res){
                expect(res.body.statusSpeakerCode).to.equal("400");
                done();
            });
    });
    it("it should return json statusSpeakerCode=400 when email wasn't sent", function(done) {
        chai.request(`http://localhost:${config.get("Server.port")}`)
            .post("/appSignUpEmailPassword")
            .send({
                "password":"testpassword"
            })
            .end(function (err, res){
                expect(res.body.statusSpeakerCode).to.equal("400");
                done();
            });
    });

});