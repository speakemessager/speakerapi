let chai = require("chai");
let chaiHttp = require("chai-http");
let mocha = require("mocha");
let expect = require("chai").expect;
let config = require("config");
let server = require("../../../../server");
let signIn = require("../../../../src/Speaker/Firebase/Account/appSignInEmailPassword");

chai.use(chaiHttp);
describe("appSignInEmailPassword", () => {

    it("it should json statusSpeakerCode=400 when password wasn't sent", function(done) {
        chai.request(`http://localhost:${config.get("Server.port")}`)
            .post("/appSignInEmailPassword")
            .send({
                "email":"test@gmail.com",
            })
            .end(function (err, res){
                expect(res.body.statusSpeakerCode).to.equal("400");
                done();
            });
    });
    it("it should json statusSpeakerCode=400 when email wasn't sent", function(done) {
        chai.request(`http://localhost:${config.get("Server.port")}`)
            .post("/appSignInEmailPassword")
            .send({
                "password":"testpassword"
            })
            .end(function (err, res){
                expect(res.body.statusSpeakerCode).to.equal("400");
                done();
            });
    });
    //TODO Rewrite test below for non-specified send, use sinon.stub
    it("it should return user json after resolve by Firebase ", function(done){

        chai.request(`http://localhost:${config.get("Server.port")}`)
            .post("/appSignInEmailPassword")
            .send({
                "email":"test@gmail.com",
                "password":"test@gmail.com"
            })
            .end(function (err, res){
                expect(res.body.code).to.be.undefined;
                done();
            });

    });
    it("it should return Error Json after thrown error by Firebase ", function(done){

        chai.request(`http://localhost:${config.get("Server.port")}`)
            .post("/appSignInEmailPassword")
            .send({
                "email":"fake@gajsnmnk.er",
                "password":"fake@fake.com"
            })
            .end(function (err, res){
                expect(res.body.statusSpeakerCode).to.equal("403");
                expect(res.body.errorSpeakerCode).to.equal("There is no user record corresponding to this identifier. The user may have been deleted.");
                done();
            });

    })


});