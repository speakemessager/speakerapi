const config = require("config");
const chai = require("chai");
const expect = require("chai").expect;
const mocha = require("mocha");
const io = require("socket.io-client");
const server = require("../../../server");


let socketUrl = `${config.get("Server.host")}:${config.get("Server.port")}`;
let options ={
    transports: ["websocket"],
    "force new connection": true
};

describe("webSocketsServerTest", () => {

    describe("VkLongPoll", ()=> {
        it("it should emit newVkUpdates with updatesArray within timeout 30 sec", function(done) {
            this.timeout(30000);

            let client1 = io.connect(socketUrl,options);

            client1.on("connect", function(){
                client1.emit("startGettingVkUpdates", {accessToken: config.get("testAccount.Vk.accessToken")})
            });

            client1.on("newVkUpdate", (data)=>{
                expect(data).to.be.a('string');
                expect(data).to.include("{\"ts\":").and.to.include("\"pts\":");
                done();
            });
        });
    })

});