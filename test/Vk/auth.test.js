let chai = require("chai");
let chaiHttp = require("chai-http");
let mocha = require("mocha");
let expect = require("chai").expect;
let config = require("config");
chai.use(chaiHttp);
describe("VkSignInAuth", () => {

    it("it should return json statusSpeakerCode=400 when userPassword wasn't sent", function(done) {
        chai.request(`http://localhost:${config.get("Server.port")}`)
            .post("/vkLogIn")
            .send({
                "userLogin":"test@gmail.com",
            })
            .end(function (err, res){
                expect(res.body.statusSpeakerCode).to.equal("400");
                done();
            });
    });
    it("it should return json statusSpeakerCode=400 when userLogin wasn't sent", function(done) {
        chai.request(`http://localhost:${config.get("Server.port")}`)
            .post("/vkLogIn")
            .send({
                "userPassword":"testpassword"
            })
            .end(function (err, res){
                expect(res.body.statusSpeakerCode).to.equal("400");
                done();
            });
    });
    it("it should return json statusSpeakerCode=403 when login is wrong", function(done){
        this.timeout(5000);
        chai.request(`http://localhost:${config.get("Server.port")}`)
            .post("/vkLogIn")
            .send({
                "userLogin":"testdncnwejnrwejn@gmail.com",
                "userPassword":"ASnvnjwernj31k23"
            })
            .end(function (err, res){
                expect(res.body.statusSpeakerCode).to.equal("403");
                done();
            });

    });
    it("it should has valid accessToken in json if authorization was succeed", function(done){
        this.timeout(5000);
        chai.request(`http://localhost:${config.get("Server.port")}`)
            .post("/vkLogIn")
            .send({
                "userLogin": config.get("testAccount.Vk.userName"),
                "userPassword": config.get("testAccount.Vk.userPassword")
            })
            .end(function (err, res){
                expect(res).to.be.json;
                expect(res.body.statusSpeakerCode).to.equal("200");
                expect(res.body).to.have.deep.property("access_token");
                updateExistedValueJson("../../config/default.json", "testAccount:Vk:accessToken", res.body.access_token, done);
            });

    });


});

function updateExistedValueJson(filePath,  key, value, callback){
    const fs = require("fs");
    const path = require("path");
    fs.readFile(path.join(__dirname, filePath), (err, data) =>{
        if (err) console.log(err);
        let i = 0;
        let goDeeperArr = key.split(":");
        let jsonObj = JSON.parse(data);
        let initialObj = jsonObj;
        for ( ;i<goDeeperArr.length-1;i++){
            jsonObj = jsonObj[goDeeperArr[i]]
        }
        jsonObj[goDeeperArr[i]] = value;

        fs.writeFile(path.join(__dirname, filePath), JSON.stringify(initialObj, null, 2), function (err) {
            if (err) return console.log(err);
            if (callback) callback();
        });

    });

};